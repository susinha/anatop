#!/bin/bash

rm -r build
mkdir build

setupATLAS
cd build
asetup AnalysisBase,21.2.170,here
cmake ../source
cmake --build ./
source */setup.sh
cd ../run

lsetup panda
export RUCIO_ACCOUNT=$CERN_USER
lsetup "rucio -w"
lsetup pyami
