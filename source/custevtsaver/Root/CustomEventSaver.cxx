/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/CustomEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopEvent/EventTools.h"
#include <TRandom3.h>
#include "xAODJet/JetContainer.h"

namespace top{
  ///-- Constrcutor --///
  CustomEventSaver::CustomEventSaver() :
    m_randomNumber(0.),
    m_someOtherVariable(0.),
    m_jet_FracSamplingMax(0.),
    m_jet_SumPtTrkPt500(0.),
    m_jet_ptOfTruthJet(0.)
  {
  }
  
  ///-- initialize - done once at the start of a job before the loop over events --///
  void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {
    m_topconfig=config;
    ///-- Let the base class do all the hard work --///
    ///-- It will setup TTrees for each systematic with a standard set of variables --///
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
    
    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {
      systematicTree->makeOutputVariable(m_randomNumber, "randomNumber");
      systematicTree->makeOutputVariable(m_someOtherVariable,"someOtherVariable");
      systematicTree->makeOutputVariable(m_jet_FracSamplingMax,"jet_FracSamplingMax");
      systematicTree->makeOutputVariable(m_jet_SumPtTrkPt500,"jet_SumPtTrkPt500");
      systematicTree->makeOutputVariable(m_jet_ptOfTruthJet,"jet_ptOfTruthJet");
    }
  }
  
  ///-- saveEvent - run for every systematic and every event --///
  void CustomEventSaver::saveEvent(const top::Event& event) 
  {
    ///-- set our variables to zero --///
    m_randomNumber = 0.;
    m_someOtherVariable = 0.;
    m_jet_FracSamplingMax.clear();
    m_jet_SumPtTrkPt500.clear();
    m_jet_ptOfTruthJet.clear();
    
    //retrieving containers now
    //    m_truthJets = 0;
    //top::check(evtStore()->retrieve( m_truthJets, m_topconfig->sgKeyTruthJets()), "Failed to retrieve truth jets");

    ///-- Fill them - you should probably do something more complicated --///
    TRandom3 random( event.m_info->eventNumber() );
    m_randomNumber = random.Rndm();    
    m_someOtherVariable = 42;

    for(const xAOD::Jet* jet : event.m_jets)
      {
	float fracsammax=-999.;
	if(jet->isAvailable<float>("FracSamplingMax")) fracsammax=jet->auxdata<float>("FracSamplingMax");
	m_jet_FracSamplingMax.push_back(fracsammax);
      }

    //    size_t sj = m_jets->size();

    //m_jet_SumPtTrkPt500.resize(sj);
    //    for (size_t i = 0; i < sj; i++) {

    for(const xAOD::Jet* jet : event.m_jets){
      std::vector<float> trkPtSumVec = jet->getAttribute<std::vector<float> >("SumPtTrkPt500");
      float trkPtSum = trkPtSumVec.size() > 0 ? trkPtSumVec[0] : 0;
      m_jet_SumPtTrkPt500.push_back(trkPtSum); //[i] = trkPtSum;

      /*      const xAOD::Jet* associated_truth_jet=0;
      float minDR=0.1;

      for(const xAOD::Jet* tjet : *m_truthJets) //we loop on the truth-jets we retrieved in the initEvent method at the beginning of the event
	{
	  if(tjet->pt()<10000.) continue; //we cannot look at truth-jets with a too low-pt, it doesn't make sense
	  float DR=tjet->p4().DeltaR(jet->p4()); //we calculate the angular distance DR(reco-jet,truth-jet)
	  if(DR<minDR) //if the truth-jet is the closest one, we store it in the pointer we prepared (and we store the minimum DR found until now)
	    {
	      minDR=DR;
	      associated_truth_jet=tjet;
	    }

	}//end of loop on truth jets
	m_jet_ptOfTruthJet.push_back(associated_truth_jet ? associated_truth_jet->pt() : -999); */
    

      //finally we fill the output vector; remember we have to check if the pointer is not 0
      //the pointer could be 0 if we found no truth-jet within dR=0.4 from the reco-jet
      //the ? : construct is equivale equivalent to a (longer)
      //float tjet_pt=-999;
      //if(associated_truth_jet) tjet_pt=associated_truth_jet->pt();
      //else tjet_pt=-999;
      //m_jet_ptOfAssociatedTruthJet.push_back(tjet_pt);
      
    }//end of loop on jets
    
    ///-- Let the base class do all the hard work --///
    top::EventSaverFlatNtuple::saveEvent(event);
  }
  
}
