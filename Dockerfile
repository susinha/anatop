FROM atlas/analysisbase:21.2.170
ADD . /analysis
WORKDIR /analysis/build
RUN sudo chown -R atlas /analysis && \
    source ~/release_setup.sh && \
    cmake ../source && \
    cmake --build ./
