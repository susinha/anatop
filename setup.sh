export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS
lsetup git
cd source/
asetup --restore
cd ../build
source */setup.sh
cd ..

export RUCIO_ACCOUNT=$CERN_USER
lsetup rucio
lsetup panda
lsetup pyami
voms-proxy-init --voms atlas
